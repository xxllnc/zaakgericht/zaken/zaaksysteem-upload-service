﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace ZaaksysteemUploadTests.Mocks
{
    class MockHttpMessageHandler : HttpMessageHandler
    {
        public HttpMethod RequestMethod { get; set; }
        public MediaTypeHeaderValue RequestContentType { get; set; }
        public string RequestBody { get; set; }

        public HttpResponseMessage Response { get; set; }

        public MockHttpMessageHandler()
        {
            Response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK
            };
        }

        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            return Task<HttpResponseMessage>.Factory.StartNew(() =>
            {
                RequestMethod = request.Method;
                RequestContentType = request.Content.Headers.ContentType;

                var reader = request.Content.ReadAsStringAsync();
                reader.Wait();
                RequestBody = reader.Result;

                return Response;
            });
        }
    }
}
