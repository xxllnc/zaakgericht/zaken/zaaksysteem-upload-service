﻿#region Software License
/*
    Copyright (c) 2015, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Collections.Generic;
using System.Linq;
using ZaaksysteemUpload;

namespace ZaaksysteemUploadService
{
    public class ServiceConfiguration : IServiceConfiguration
    {
        internal Properties.ISettings Settings;

        public ServiceConfiguration(Properties.ISettings s)
        {
            Settings = s;

            if (Settings.UpgradeRequired)
            {
                Settings.Upgrade();

                // Fill in "Valid" as a boolean array, all false.
                Settings.Valid = Enumerable.Repeat(false, Settings.Name.Count).ToArray();

                Settings.UpgradeRequired = false;
                Settings.Save();
            }
        }

        public uint HTTPTimeoutMinutes
        {
            get { return Settings.HTTPTimeoutMinutes; }
        }

        public List<ConfigurationDataItem> GetConfigurationItems()
        {
            var ci = new List<ConfigurationDataItem>();

            for (var i = 0; i < Settings.Name.Count; i++)
            {
                var ConfigItem = new ConfigurationDataItem
                {
                    Name = Settings.Name[i],
                    APIKey = Settings.APIKey[i],
                    PostURL = Settings.PostURL[i],
                    WatchPath = Settings.WatchPath[i],
                    Valid = Settings.Valid[i]
                };

                ci.Add(ConfigItem);
            }

            return ci;
        }

        public void SetConfiguration(ConfigurationData config)
        {
            var validList = new List<bool>();
            Settings.Name.Clear();
            Settings.APIKey.Clear();
            Settings.PostURL.Clear();
            Settings.WatchPath.Clear();

            foreach (var cdi in config.ConfigurationItems)
            {
                Settings.Name.Add(cdi.Name);
                Settings.APIKey.Add(cdi.APIKey);
                Settings.PostURL.Add(cdi.PostURL);
                Settings.WatchPath.Add(cdi.WatchPath);
                validList.Add(cdi.Valid);
            }

            Settings.Valid = validList.ToArray();
            Settings.HTTPTimeoutMinutes = config.HTTPTimeoutMinutes;
            Settings.Save();

            return;
        }

    }
}
