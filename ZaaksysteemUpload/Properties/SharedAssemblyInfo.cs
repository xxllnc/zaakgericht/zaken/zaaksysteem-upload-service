#region Software License
/*
    Copyright (c) 2015-2016, Mintlab B.V. and all the persons listed in the
    CONTRIBUTORS file.

    Zaaksysteem Upload Service uses the EUPL license, for more information
    please have a look at the LICENSE file.
*/
#endregion

using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Zaaksysteem Upload Service")]
[assembly: AssemblyDescription(@"Zaaksysteem Upload Service uses the EUPL license.

For more information please have a look at the LICENSE file.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mintlab B.V.")]
[assembly: AssemblyProduct("Zaaksysteem Upload Service")]
[assembly: AssemblyCopyright("Copyright �  2015-2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]